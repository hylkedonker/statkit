# Changelog

All notable changes to this project will be documented in this file.


## [Unreleased]

### Added
-

### Changed
-

### Deprecated
-

### Removed
-
### Fixed
-

### Security
-

## [1.0.0]

### Added
- `statkit.dataset.split_multinomial_dataset`: renamed from `statkit.model_selection.holdout_split`.
- `statkit.dataset.balanced_downsample`: renamed from `statkit.dataset.stratified_downsample`.

### Changed
- All `statkit.non_parametric` functions: `**kwargs` parameter now replaces `metric_kwargs`:


### Removed
- `statkit.model_selection.holdout_split`: see Added.
